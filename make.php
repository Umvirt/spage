#!/usr/bin/env php
<?php
include "config.php";
$inputdir='input/'.$config['input'].'/';
$templatedir='templates/'.$config['template'].'/';
include $inputdir."site.php";
//var_dump ($inputdir."config.php");


class WebPage{
public $file;
public $title;
public $label;
}


function menu($pages, $cpage){
$s="";
foreach ($pages as $page){
if($page->file==$cpage->file){
$s.="<li><b>".$page->label."</b></li>";
}else{
$s.="<li><a href=".$page->file.">".$page->label."</a></li>";
}
}
return "<ul>$s</ul>";
}

$pages=array();
$filesList=File($inputdir.'/files.txt');
foreach($filesList as $file_){
if(trim($file_)){
$file=explode("\t",trim($file_));
//var_dump($file);

$webpage=new webpage;
$webpage->file=$file[0];
$webpage->title=$file[1];
$webpage->label=$file[2];
$pages[]=$webpage;
}
}

$template=file_get_contents($templatedir.'/template.tpl');
//var_dump($pages, $template);

foreach ($pages as $page){
$content=$template;
$content=str_replace("%headtitle%", $config['sitename']." - ".$page->title, $content);

$content=str_replace("%title%", $page->title, $content);
$content=str_replace("%sitename%", $config['sitename'], $content);
$content=str_replace("%slogan%", $config['slogan'], $content);

$content=str_replace("%copyright%", $config['copyright'], $content);
$content=str_replace("%YEAR%", date("Y"), $content);

$content=str_replace("%body%", file_get_contents($inputdir.$page->file),$content);
$content=str_replace("%menu%",menu($pages,$page),$content);
file_put_contents("output/".$page->file,$content);
}
